//
//  PID.swift
//  F0T0
//
//  Created by Martin Pichlmair on 04.14.15.
//  Copyright (c) 2015 Sunset Lake Software. All rights reserved.
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import Foundation

class PID {
	var setpoint: Double = 0.0
	var Ki: Double = 0.5
	var Kd: Double = 0.5
	var Kp: Double = 0.5
	var integral: Double = 0.0
	var previous_error: Double = 0.0
	var derivative: Double = 0.0
	
	convenience init() {
		self.init(i:0.5,d:0.5,p:0.5)
	}
	
	init(i: Double, d: Double, p: Double) {
		Ki = i
		Kd = d
		Kp = p
	}
	
	func addPID(point: Double, dt: Double) -> Double {
		let error = setpoint - point
		integral = integral + error*dt
		derivative = (error - previous_error)/dt
		let output: Double = Kp*error + Ki*integral + Kd*derivative
		previous_error = error
		return output
	}
}
