//
//  Created by Martin Pichlmair on 03.27.15.
//  Copyright (c) 2015 Broken Rules. All rights reserved.
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import UIKit

/* Google Analytics Tracking IDs:
	HPST: UA-61247883-1
	BLCK: UA-61247883-2
	MGNT: UA-61247883-3
*/

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
                            
    var window: UIWindow?
	var suspended: Bool = false
	
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject : AnyObject]?) -> Bool {
		
		GAI.sharedInstance().trackUncaughtExceptions = true
		GAI.sharedInstance().dispatchInterval = 20
//		GAI.sharedInstance().logger.logLevel = GAILogLevel.Verbose
		
		var tracker: GAITracker
		#if HPST
			tracker = GAI.sharedInstance().trackerWithTrackingId("UA-61247883-1")
		#endif
		#if BLCK
			tracker = GAI.sharedInstance().trackerWithTrackingId("UA-61247883-2")
		#endif
		#if MGNT
			tracker = GAI.sharedInstance().trackerWithTrackingId("UA-61247883-3")
		#endif
		#if NVRS
			tracker = GAI.sharedInstance().trackerWithTrackingId("UA-61247883-4")
		#endif
		
		GAI.sharedInstance().defaultTracker.send(
			GAIDictionaryBuilder.createScreenView().build() as [NSObject : AnyObject]
		)
		
		UIDevice.currentDevice().beginGeneratingDeviceOrientationNotifications()
		NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(AppDelegate.orientationChanged), name: UIDeviceOrientationDidChangeNotification, object: nil)
		
        return true
    }
	
	func applicationDidEnterBackground(application: UIApplication) {
		suspended = true
		UIDevice.currentDevice().endGeneratingDeviceOrientationNotifications()
	}
	
	func applicationDidBecomeActive(application: UIApplication) {
		
		if (suspended) {
			UIDevice.currentDevice().beginGeneratingDeviceOrientationNotifications()
			let viewController = (application.keyWindow?.rootViewController as! ViewController)
			viewController.loadDefaults()
			viewController.applyDefaults()
			viewController.activatePreset()
		}
	}

	func applicationWillTerminate(application: UIApplication) {
		UIDevice.currentDevice().endGeneratingDeviceOrientationNotifications()
	}
	
	func orientationChanged() {
		let viewController = (window?.rootViewController as! ViewController)
		viewController.orientationChanged();
	}
	

}
