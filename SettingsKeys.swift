//
//  Created by Martin Pichlmair on 03.27.15.
//  Copyright (c) 2015 Broken Rules. All rights reserved.
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import Foundation

struct SettingsKeys {
    static let helpPreferenceKey: String = "help_enabled"
	static let hdPreferenceKey: String = "hd_enabled"
	static let boldPreferenceKey: String = "bold_enabled"
	static let autoShootPreferenceKey: String = "autoshoot_enabled"
	static let autoShootFactorPreferenceKey: String = "autoshoot_factor"
	static let forceHorizontalPreferenceKey: String = "horizontal_enabled"
	static let forceHorizontalFactorPreferenceKey: String = "horizontal_factor"
}
