//
//  LocationHandler.swift
//  F0T0
//
//  Created by Martin Pichlmair on 04.05.15.
//  Copyright (c) 2015 Broken Rules. All rights reserved.
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import Foundation
import CoreLocation

class LocationHandler : NSObject, CLLocationManagerDelegate {

	var lastLocation = CLLocation()
	var locationManager: CLLocationManager!
	var seenError : Bool = false
	var locationServicesActive : Bool = false

	override init() {
		super.init()
		initLocationManager()
	}
	
	// Location Manager helper stuff
	func initLocationManager() {
		seenError = false
		locationManager = CLLocationManager()
		locationManager.delegate = self
		locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
		locationManager.requestWhenInUseAuthorization()
	}

	// Location Manager Delegate stuff
	func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
		locationManager.stopUpdatingLocation()
	}

	func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
		let locationArray = locations as NSArray
		let locationObj = locationArray.lastObject as! CLLocation
//		var coord = locationObj.coordinate
//		println(coord.latitude)
//		println(coord.longitude)
//		
		lastLocation = locationObj
	}

	func locationManager(manager: CLLocationManager,  didChangeAuthorizationStatus status: CLAuthorizationStatus) {
		if (status == CLAuthorizationStatus.Restricted ||
			status == CLAuthorizationStatus.Denied ||
			status == CLAuthorizationStatus.NotDetermined) {
			locationServicesActive = false
		} else {
			locationServicesActive = true
			locationManager.startUpdatingLocation()
		}
	}

}
