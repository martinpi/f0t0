//
//  CornersView.swift
//  F0T0
//
//  Created by Martin Pichlmair on 04.16.15.
//  Copyright (c) 2015 Sunset Lake Software. All rights reserved.
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import Foundation
import UIKit

class CornersView : UIView {
	var shapeLayer : CAShapeLayer?
	var corners: UIBezierPath?
	var outwards: Bool = false
	
	func appendCornersPath(path : UIBezierPath, bounds: CGRect, centre: CGPoint, size: CGFloat) -> UIBezierPath {
		let sizeX = size
		let sizeY = sizeX * 1.41
		let cradius : CGFloat = outwards ? sizeX/10.0 : -sizeX/10.0
		
		path.moveToPoint(   CGPointMake(center.x - sizeX, center.y + sizeY - cradius))
		path.addLineToPoint(CGPointMake(center.x - sizeX, center.y + sizeY))
		path.addLineToPoint(CGPointMake(center.x - sizeX + cradius, center.y + sizeY))
		
		path.moveToPoint(   CGPointMake(center.x + sizeX, center.y - sizeY + cradius))
		path.addLineToPoint(CGPointMake(center.x + sizeX, center.y - sizeY))
		path.addLineToPoint(CGPointMake(center.x + sizeX - cradius, center.y - sizeY))
		
		path.moveToPoint(   CGPointMake(center.x - sizeX, center.y - sizeY + cradius))
		path.addLineToPoint(CGPointMake(center.x - sizeX, center.y - sizeY))
		path.addLineToPoint(CGPointMake(center.x - sizeX + cradius, center.y - sizeY))
		
		path.moveToPoint(   CGPointMake(center.x + sizeX, center.y + sizeY - cradius))
		path.addLineToPoint(CGPointMake(center.x + sizeX, center.y + sizeY))
		path.addLineToPoint(CGPointMake(center.x + sizeX - cradius, center.y + sizeY))
		
		return path
	}
	func getPaths(bounds: CGRect, scale: CGFloat) -> UIBezierPath {
		var path : UIBezierPath = UIBezierPath()
		
		let center = CGPointMake(
			bounds.minX + bounds.width/2,
			bounds.minY + bounds.height/2)
		let size = min(50, min(bounds.width, bounds.height)/7.0) * scale

		path = appendCornersPath(path, bounds: bounds, centre: center, size: size)
		
		return path
	}
	
	override func layoutSubviews() {
		if (self.shapeLayer == nil)
		{
			self.shapeLayer = CAShapeLayer()
			self.shapeLayer?.bounds = self.frame
			self.shapeLayer?.position = CGPointMake(self.frame.width/2, self.frame.height/2)
			self.shapeLayer?.anchorPoint = CGPointMake(0.5, 0.5)
			self.shapeLayer?.strokeColor = UIColor.whiteColor().CGColor
			self.shapeLayer?.fillColor = UIColor.clearColor().CGColor
			self.shapeLayer?.lineWidth = 1
			self.shapeLayer?.zPosition = -1.0
			
			self.layer.addSublayer(self.shapeLayer!);
			
			self.shapeLayer?.path = getPaths(self.bounds, scale: 1.0).CGPath
		}
	}
	
	func updatePaths() {
		self.shapeLayer?.path = getPaths(self.bounds, scale: 1.0).CGPath
	}
}
