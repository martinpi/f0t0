//
//  Created by Martin Pichlmair on 03.27.15.
//  Copyright (c) 2015 Broken Rules. All rights reserved.
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import UIKit
import AssetsLibrary
import GPUImage
import CoreLocation
import ImageIO

class ViewController: UIViewController {
	
	@IBOutlet var helpViewController: InfoViewController?
	
	@IBOutlet var hudView: CornersView?
	@IBOutlet var overlayView: OverlayView?
	
	var stillCamera:GPUImageStillCamera?
	
	var filter: GPUImageOutput?
	
	var filter_exposure: GPUImageExposureFilter?
	var filter_vignette: GPUImageVignetteFilter?
	var filter_contrast: GPUImageContrastFilter?
	var filter_haze: GPUImageHazeFilter?
	
    var filter_mgnt:GPUImageRGBFilter?
	var filter_hpst:GPUImageAmatorkaFilter?
	var filter_blck:GPUImageSaturationFilter?
	
	var preset: String?
	var paused: Bool = false
	
	var autoRotate: Bool = false
	
	var showHelp: Bool = false
	var hdMode: Bool = true
	var boldMode: Bool = false
	var autoShootMode: Bool = true
	var autoShootFactor: Double = 0.5
	var forceHorizontalMode: Bool = true
	var forceHorizontalFactor: Double = 0.5
	
	var charged: Bool = false
	var timeout: Double = 1.0
	lazy var locationHandler:  LocationHandler = LocationHandler()
	lazy var motionHandler: MotionHandler = MotionHandler()
	var horizonTransform: CGAffineTransform = CGAffineTransformIdentity
	var hudTransform: CGAffineTransform = CGAffineTransformIdentity
	
	func delay(delay:Double, closure:()->()) {
		dispatch_after(
			dispatch_time(
				DISPATCH_TIME_NOW,
				Int64(delay * Double(NSEC_PER_SEC))
			),
			dispatch_get_main_queue(), closure)
	}
	
	func track(category: String, action: String, label: String, value: Float?) {
		GAI.sharedInstance().defaultTracker.send(
			GAIDictionaryBuilder.createEventWithCategory(category, action: action, label: label, value: value).build() as [NSObject : AnyObject]
		)
	}
	
	func activatePreset() {
		stillCamera?.captureSession.sessionPreset = preset
	}
	
	func pushSession() {
		if (stillCamera?.captureSession.sessionPreset != AVCaptureSessionPresetPhoto) {
			stillCamera?.captureSession.sessionPreset = AVCaptureSessionPresetPhoto
		}
	}
	func popSession() {
		if (stillCamera?.captureSession.sessionPreset != AVCaptureSessionPresetiFrame1280x720) {
			stillCamera?.captureSession.sessionPreset = AVCaptureSessionPresetiFrame1280x720
		}
	}
	
	func setLocationMetadata(location: CLLocation!) -> NSDictionary {
		// Via: https://github.com/gpambrozio/GusUtils/blob/master/GusUtils/NSMutableDictionary+ImageMetadata.m
		// From: http://stackoverflow.com/questions/3884060/need-help-in-saving-geotag-info-with-photo-on-ios4-1
	
		var exifLongitude:CLLocationDegrees = location.coordinate.longitude
		var exifLatitude:CLLocationDegrees = location.coordinate.latitude
		
		var latRef: String
		var lngRef: String
		if (exifLatitude < 0.0) {
			exifLatitude = exifLatitude * -1.0;
			latRef = "S";
		} else {
			latRef = "N";
		}
		
		if (exifLongitude < 0.0) {
			exifLongitude = exifLongitude * -1.0;
			lngRef = "W";
		} else {
			lngRef = "E";
		}
		
		let formatter: NSDateFormatter = NSDateFormatter()
		formatter.dateFormat = "yyyy:MM:dd HH:mm:ss"
		let dateString : String = formatter.stringFromDate(location.timestamp)
		
		let locDict: NSMutableDictionary = NSMutableDictionary()
		locDict.setObject(dateString, forKey:kCGImagePropertyGPSTimeStamp as NSString)
		locDict.setObject(latRef, forKey:kCGImagePropertyGPSLatitudeRef as NSString)
		locDict.setObject(NSNumber(double: exifLatitude), forKey:kCGImagePropertyGPSLatitude as NSString)
		locDict.setObject(lngRef, forKey:kCGImagePropertyGPSLongitudeRef as NSString)
		locDict.setObject(NSNumber(double: exifLongitude), forKey:kCGImagePropertyGPSLongitude as NSString)
		locDict.setObject(NSNumber(double: location.horizontalAccuracy), forKey:kCGImagePropertyGPSDOP as NSString)
		locDict.setObject(NSNumber(double: location.altitude), forKey:kCGImagePropertyGPSAltitude as NSString)
		
		return NSDictionary(dictionary: locDict)
	}
	
	func takePicture() {
		if (self.paused == true) {
			return
		}
		self.charged = false
		self.paused = true
		self.hudView?.hidden = true
		self.overlayView?.hidden = true
		
		stillCamera?.capturePhotoAsJPEGProcessedUpToFilter(filter, withOrientation: UIImageOrientation.Up, withCompletionHandler: { (saveImage : NSData!, saveError : NSError!) -> Void in
			if (saveImage == nil) {
				self.track("Usage", action: "Image", label: "Error", value: 1.0)
				return
			}
			
			let meta: [NSObject : AnyObject]? = self.stillCamera!.currentCaptureMetadata
			if (meta != nil) {
				let metaDict: NSMutableDictionary = NSMutableDictionary(dictionary: meta!)
				var orientation: Int = UIInterfaceOrientation.Portrait.rawValue
				
				/*
				UIImageOrientationUp:             1
				UIImageOrientationDown:           3
				UIImageOrientationLeft:           8
				UIImageOrientationRight:          6
				UIImageOrientationUpMirrored:     2
				UIImageOrientationDownMirrored:   4
				UIImageOrientationLeftMirrored:   5
				UIImageOrientationRightMirrored:  7
				*/
				
				switch (UIDevice.currentDevice().orientation) {
				case UIDeviceOrientation.LandscapeLeft:
					orientation = 8
					break
				case UIDeviceOrientation.LandscapeRight:
					orientation = 6
					break
				default:
					orientation = 1
					break
				}
				metaDict.setObject(NSNumber(long: orientation), forKey: kCGImagePropertyOrientation as NSString)
				
				if (self.locationHandler.locationServicesActive == true) {
					let location: CLLocation? = self.locationHandler.lastLocation
					if (location?.coordinate.longitude != 0.0 && location?.coordinate.latitude != 0.0) {
						let locDict: NSDictionary = self.setLocationMetadata(location!)
						metaDict.setObject(locDict, forKey: kCGImagePropertyGPSDictionary as NSString)
					}
				}
				
//				print(metaDict)
				
				self.stillCamera?.pauseCameraCapture()
				ALAssetsLibrary().writeImageDataToSavedPhotosAlbum(saveImage, metadata: metaDict as [NSObject : AnyObject],
					completionBlock: { (assetURL: NSURL!, error: NSError!) -> Void in
						
						self.track("Usage", action: "Image", label: "Taken", value: 1.0)
						
						self.delay(self.timeout) {
							self.paused = false
							self.stillCamera?.resumeCameraCapture()
							self.hudView?.hidden = false
							self.overlayView?.hidden = (self.autoShootMode || self.forceHorizontalMode)
						}

						return
				})
			}
		})
		
	}
	
	func saveFrame() {
		var saveImage : UIImage!
		filter?.useNextFrameForImageCapture()
		saveImage = filter?.imageFromCurrentFramebuffer()
		
		if (saveImage != nil) {
			let orientation : ALAssetOrientation = ALAssetOrientation(rawValue: saveImage.imageOrientation.rawValue)!
			ALAssetsLibrary().writeImageToSavedPhotosAlbum(saveImage.CGImage, orientation: orientation, completionBlock: nil)
		}
	}
	
	override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
		if (event!.allTouches()?.count == 2 && hdMode == false) {
			pushSession()
		}
		if (autoShootMode || forceHorizontalMode) {
			charged = true
		}
		self.overlayView!.hidden = false
		self.hudView!.outwards = true
		self.hudView!.updatePaths()
		super.touchesBegan(touches, withEvent: event)
	}
	override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
		if (!autoShootMode && !forceHorizontalMode) {
			takePicture()
		}
		if (hdMode == false) {
			popSession()
		}
		charged = false
		self.overlayView!.hidden = (autoShootMode || forceHorizontalMode)
		self.hudView!.outwards = false
		self.hudView!.updatePaths()
		super.touchesEnded(touches, withEvent: event)
	}
	override func touchesCancelled(touches: Set<UITouch>?, withEvent event: UIEvent?) {
		if (hdMode == false) {
			popSession()
		}
		charged = false
		self.overlayView!.hidden = (autoShootMode || forceHorizontalMode)
		self.hudView!.outwards = false
		self.hudView!.updatePaths()
		super.touchesCancelled(touches, withEvent: event)
	}
	
	func loadDefaults() {
		let appDefaults = [SettingsKeys.helpPreferenceKey: false,
			SettingsKeys.hdPreferenceKey: true,
			SettingsKeys.boldPreferenceKey: false,
			SettingsKeys.autoShootPreferenceKey: true,
			SettingsKeys.autoShootFactorPreferenceKey: 0.5,
			SettingsKeys.forceHorizontalPreferenceKey: true,
			SettingsKeys.forceHorizontalFactorPreferenceKey: 0.5 ]
		NSUserDefaults.standardUserDefaults().registerDefaults(appDefaults)
		showHelp = NSUserDefaults.standardUserDefaults().boolForKey(SettingsKeys.helpPreferenceKey)
		hdMode = NSUserDefaults.standardUserDefaults().boolForKey(SettingsKeys.hdPreferenceKey)
		boldMode = NSUserDefaults.standardUserDefaults().boolForKey(SettingsKeys.boldPreferenceKey)
		autoShootMode = NSUserDefaults.standardUserDefaults().boolForKey(SettingsKeys.autoShootPreferenceKey)
		autoShootFactor = NSUserDefaults.standardUserDefaults().doubleForKey(SettingsKeys.autoShootFactorPreferenceKey)
		forceHorizontalMode = NSUserDefaults.standardUserDefaults().boolForKey(SettingsKeys.forceHorizontalPreferenceKey)
		forceHorizontalFactor = NSUserDefaults.standardUserDefaults().doubleForKey(SettingsKeys.forceHorizontalFactorPreferenceKey)
	}
	
	func saveDefaults() {
		NSUserDefaults.standardUserDefaults().setValue(showHelp, forKey:SettingsKeys.helpPreferenceKey)
		NSUserDefaults.standardUserDefaults().setValue(hdMode, forKey:SettingsKeys.hdPreferenceKey)
		NSUserDefaults.standardUserDefaults().setValue(boldMode, forKey:SettingsKeys.boldPreferenceKey)
		NSUserDefaults.standardUserDefaults().setValue(autoShootMode, forKey:SettingsKeys.autoShootPreferenceKey)
		NSUserDefaults.standardUserDefaults().setValue(autoShootFactor, forKey:SettingsKeys.autoShootFactorPreferenceKey)
		NSUserDefaults.standardUserDefaults().setValue(forceHorizontalMode, forKey:SettingsKeys.forceHorizontalPreferenceKey)
		NSUserDefaults.standardUserDefaults().setValue(forceHorizontalFactor, forKey:SettingsKeys.forceHorizontalFactorPreferenceKey)
		NSUserDefaults.standardUserDefaults().synchronize()
	}
	
	func applyDefaults() {
		preset = AVCaptureSessionPresetiFrame1280x720
		if (hdMode == true) {
			preset = AVCaptureSessionPresetPhoto
		}
	}
	func applyViewDefaults() {
		// subviews need to be present at this point
		if (boldMode) {
			overlayView!.shapeLayer?.lineWidth = 2
			hudView!.shapeLayer?.lineWidth = 2
		} else {
			overlayView!.shapeLayer?.lineWidth = 1
			hudView!.shapeLayer?.lineWidth = 1
		}
	}
	
	func orientationChanged() {
		if (!autoRotate) {
			return
		}
		
		let orientation: UIDeviceOrientation = UIDevice.currentDevice().orientation;
		switch (orientation) {
			case UIDeviceOrientation.Portrait:
			stillCamera!.outputImageOrientation = UIInterfaceOrientation.Portrait;
			break;
			case UIDeviceOrientation.LandscapeLeft:
			stillCamera!.outputImageOrientation = UIInterfaceOrientation.LandscapeRight;
			break;
			case UIDeviceOrientation.LandscapeRight:
			stillCamera!.outputImageOrientation = UIInterfaceOrientation.LandscapeLeft;
			break;
		default:
			break;
		}
	}
	
	override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
		return autoRotate ? UIInterfaceOrientationMask.AllButUpsideDown : UIInterfaceOrientationMask.Portrait
	}

	override func shouldAutorotate() -> Bool {
		return autoRotate
	}
	
	func updateAngle() {
		self.overlayView!.transform = CGAffineTransformRotate(horizonTransform, -CGFloat(motionHandler.angle+M_PI/2.0));
		
		var angleCorrection = M_PI/2.0
		var hudAngleCorrection = 0.0
		switch (UIDevice.currentDevice().orientation) {
			case UIDeviceOrientation.LandscapeLeft:
				angleCorrection = -M_PI
				hudAngleCorrection = -M_PI/2.0
			break
			case UIDeviceOrientation.LandscapeRight:
				angleCorrection = 0
				hudAngleCorrection = M_PI/2.0
			break
			default:
			break
		}
		self.hudView!.transform = CGAffineTransformRotate(hudTransform, CGFloat(hudAngleCorrection));
		
		self.overlayView!.horizonAngle = CGFloat(motionHandler.angle+angleCorrection)
		self.overlayView!.stabilityFactor = 1.0-CGFloat(min(max((motionHandler.stability-0.0005)*25.0, 0.0), 1.0))
		self.overlayView!.updatePaths()
		
		let autoShootStability: Bool = motionHandler.stability < 0.0015 * (1.1 - autoShootFactor)
		let horizontal: Bool = abs(overlayView!.horizonAngle) < CGFloat(0.02 * (1.1 - forceHorizontalFactor))
		
		if ((charged &&
			(!autoShootMode || autoShootStability )) &&
			(!forceHorizontalMode || horizontal )) {
				takePicture()
		}
	}
	
	override func viewDidAppear(animated: Bool) {
		if (showHelp) {
			self.performSegueWithIdentifier("ShowInfoView", sender: self)
			showHelp = false
			saveDefaults()
		}
		applyViewDefaults()
	}
	
    override func viewDidLoad() {
        super.viewDidLoad()
		
		loadDefaults()
		applyDefaults()
		
		if UIDevice.currentDevice().model == "iPhone Simulator" || UIDevice.currentDevice().model == "iPad Simulator" {
			return
		}
		
		track("Usage", action: "Startup", label: UIDevice.currentDevice().model, value: 1.0)
		track("Usage", action: "Configuration", label: "HD Mode", value: hdMode == true ? 1 : 0)
		track("Usage", action: "Configuration", label: "Force Steady", value: autoShootMode == true ? 1 : 0)
		track("Usage", action: "Configuration", label: "Force Horizontal", value: forceHorizontalMode == true ? 1 : 0)
		
		horizonTransform = self.overlayView!.transform
		hudTransform = self.hudView!.transform
		
		self.overlayView!.hidden = (autoShootMode || forceHorizontalMode)
		
		stillCamera = GPUImageStillCamera(sessionPreset: preset, cameraPosition: AVCaptureDevicePosition.Back)
		stillCamera!.outputImageOrientation = .Portrait;

		filter_exposure = GPUImageExposureFilter()

		filter_vignette = GPUImageVignetteFilter()
		filter_vignette?.vignetteStart = 0.6
		filter_vignette?.vignetteEnd = 0.9

		#if NVRS
			stillCamera?.addTarget(filter_exposure)
			
			filter_blck = GPUImageSaturationFilter()
			filter_blck?.saturation = 1.1
			
			filter_contrast = GPUImageContrastFilter()
			filter_contrast?.contrast = 1.5
			
			filter_exposure?.exposure = -0.05
			
			filter_exposure?.addTarget(filter_contrast)
			filter_contrast?.addTarget(filter_blck)
			
			filter = filter_blck
			filter?.addTarget(self.view as! GPUImageView)
		#else
			stillCamera?.addTarget(filter_exposure)
			#if MGNT
				filter_exposure?.exposure = -0.3
				
				filter_mgnt = GPUImageRGBFilter()
				filter_mgnt?.red = 1.0
				filter_mgnt?.green = 0.9
				filter_mgnt?.blue = 1.0

				filter_exposure?.exposure = -0.15
				
				filter_contrast = GPUImageContrastFilter()
				filter_contrast?.contrast = 3.0
				
				filter_blck = GPUImageSaturationFilter()
				filter_blck?.saturation = 0.5
				
				filter_exposure?.addTarget(filter_contrast)
				filter_contrast?.addTarget(filter_blck)
				filter_blck?.addTarget(filter_mgnt)
				
				filter = filter_mgnt
				filter?.addTarget(self.view as! GPUImageView)

			#else
				
				#if BLCK
					filter_exposure?.exposure = -0.45
					
					filter_contrast = GPUImageContrastFilter()
					filter_contrast?.contrast = 2.0
						
					filter_blck = GPUImageSaturationFilter()
					filter_blck?.saturation = 0.0
					
					filter_exposure?.addTarget(filter_contrast)
					filter_contrast?.addTarget(filter_vignette)
					filter_vignette?.addTarget(filter_blck)
					
					filter = filter_blck
					filter?.addTarget(self.view as! GPUImageView)

				#else
					filter_exposure?.exposure = -0.1
					
					filter_hpst = GPUImageAmatorkaFilter()
						
					filter_exposure?.addTarget(filter_hpst)
					filter_hpst?.addTarget(filter_vignette)
						
					filter = filter_vignette
					filter_vignette?.addTarget(self.view as! GPUImageView)
				#endif
			#endif
		#endif
		
        stillCamera?.startCameraCapture()
		
		NSTimer.scheduledTimerWithTimeInterval(0.05, target: self, selector: #selector(ViewController.updateAngle), userInfo: nil, repeats: true)
		
    }
}
